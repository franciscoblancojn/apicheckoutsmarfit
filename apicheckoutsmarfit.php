<?php
/*
Plugin Name: API para Checkout Smarfit
Plugin URI: https://startscoinc.com/es/
Description: Genera una API personalizada para manejo de productos compras y clientes.
Author: Startsco
Version: 1.0.8
Author URI: https://startscoinc.com/es/#
License: 
*/

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/franciscoblancojn/apicheckoutsmarfit',
	__FILE__,
	'apicheckoutsmarfit'
);
$myUpdateChecker->setAuthentication('MqiAAZzo9WNBAGzgwc3L');
$myUpdateChecker->setBranch('master');

require_once plugin_dir_path(__FILE__) . "admin/class-option-page.php";
require_once plugin_dir_path(__FILE__) . "includes/class-products.php";
require_once plugin_dir_path(__FILE__) . "includes/class-users.php";
require_once plugin_dir_path(__FILE__) . "includes/class-api.php";

function activate_WP_API_Checkout()
{
    require_once plugin_dir_path(__FILE__) . 'includes/class-activation.php';
    $WPAC_Activator = new WPAC_Activator();
    $WPAC_Activator->WPAC_Activate();
}
register_activation_hook(__FILE__, 'activate_WP_API_Checkout');

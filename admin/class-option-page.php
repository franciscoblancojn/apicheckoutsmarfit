<?php

class WPAC_Register_Options_Page
{
    function __construct()
    {
        add_action('admin_menu', array($this, 'wpac_function_options_page'));
    }

    function wpac_function_options_page()
    {
        add_options_page('API Checkout', 'API Checkout', 'manage_options', 'wpac', array($this, 'wpac_tempate_options_page'));
    }

    function wpac_tempate_options_page()
    {
        require_once plugin_dir_path(__FILE__) . 'template/template-options-page.php';
    }
}

new WPAC_Register_Options_Page;

<?php $clientKey = get_option('WPAC_Client_Key'); ?>
<?php $secretKey = get_option('WPAC_Secret_Key'); ?>
<?php $urlCheckout = get_option('WPAC_URL_Checkout'); ?>

<h3>API Checkout</h3>
<p>Para ver y testear la API de Checkout puede revisar nuestro <a href="#">swagger aquí</a></p>

<div>
    <label for="key">URL Checkout</label>
    <input type="text" name="key" id="key" value="<?php echo $urlCheckout; ?>" readonly>
    <input type="button" value="Copiar" class="copy">
</div>

<div>
    <label for="key">Client Key</label>
    <input type="text" name="key" id="key" value="<?php echo $clientKey; ?>" readonly>
    <input type="button" value="Copiar" class="copy">
</div>

<div>
    <label for="key">Secret Key</label>
    <input type="text" name="key" id="key" value="<?php echo $secretKey; ?>" readonly>
    <input type="button" value="Copiar" class="copy">
</div>
<script type="text/javascript">
    document.querySelectorAll('.copy').forEach(el => el.addEventListener('click', copy))

    function copy(e) {
        const input = this.previousElementSibling;
        input.select();
        document.execCommand('copy');
    }
</script>
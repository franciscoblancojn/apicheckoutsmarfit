# WP API Checkout

Este plugin maneja los metodos de la API para consumo externo.

## includes/class-activation.php
Este metodo es para activar la validacion del plugin.

## includes/class-api.php
Este metodo es para activar la validacion del plugin y sus endpoints.

## includes/class-products.php
Este archivo maneja todos los metodos de manejo de productos.

## includes/class-users.php
Este archivo maneja todos los metodos de manejo de usuarios.


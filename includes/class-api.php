<?php

/** CLASE PARA GENERAR LA API EN WORDPRESS */
class WPAC_Custom_API
{
    function __construct()
    {
        add_action('rest_api_init', array($this, 'wpac_custom_api_function'));
    }

    /**
     * wpac_custom_api_function crea la API y sus respectivos endpoints
     * 
     * @access public
     * @return string
     */
    function wpac_custom_api_function()
    {
        register_rest_route('wpac/v1', '/product/create/', array(
            'methods'   =>  'POST',
            'callback'  =>  array($this, 'WPAC_MethodCreateProduct'),
            'permission_callback' => '__return_true'
        ));
        register_rest_route('wpac/v1', '/product/update/', array(
            'methods'   =>  'POST',
            'callback'  =>  array($this, 'WPAC_MethodUpdateProduct'),
            'permission_callback' => '__return_true'
        ));
        register_rest_route('wpac/v1', '/product/read/', array(
            'methods'   =>  'POST',
            'callback'  =>  array($this, 'WPAC_MethodReadProduct'),
            'permission_callback' => '__return_true'
        ));
        register_rest_route('wpac/v1', '/user/create/', array(
            'methods'   =>  'POST',
            'callback'  =>  array($this, 'WPAC_MethodCreateUser'),
            'permission_callback' => '__return_true'
        ));
        register_rest_route('wpac/v1', '/user/read/', array(
            'methods'   =>  'POST',
            'callback'  =>  array($this, 'WPAC_MethodReadUser'),
            'permission_callback' => '__return_true'
        ));
        register_rest_route('wpac/v1', '/user/edit/', array(
            'methods'   =>  'POST',
            'callback'  =>  array($this, 'WPAC_MethodEditUser'),
            'permission_callback' => '__return_true'
        ));
        register_rest_route('wpac/v1', '/user/orders/', array(
            'methods'   =>  'POST',
            'callback'  =>  array($this, 'WPAC_MethodReadOrdersUser'),
            'permission_callback' => '__return_true'
        ));
        register_rest_route('wpac/v1', '/user/orders/cancelled', array(
            'methods'   =>  'POST',
            'callback'  =>  array($this, 'WPAC_MethodCancelSuscriptionUser'),
            'permission_callback' => '__return_true'
        ));
        register_rest_route('wpac/v1', '/user/user/recover_password', array(
            'methods'   =>  'POST',
            'callback'  =>  array($this, 'WPAC_MethodRecoverPassword'),
            'permission_callback' => '__return_true'
        ));
        register_rest_route('wpac/v1', '/user/user/signup', array(
            'methods'   =>  'POST',
            'callback'  =>  array($this, 'WPAC_SignUpUser'),
            'permission_callback' => '__return_true'
        ));
    }

    /**
     * WPAC_MethodCreateProduct Endpoint para Crear Producto
     * 
     * @access public
     * @return string
     */
    function WPAC_MethodCreateProduct($request)
    {
        // Obtengo el Body del Request
        $data = $request->get_body();
        // Convierto el Body del Request en Array
        $decoded = json_decode($data);

        $WPAC_Products = new WPAC_Products();
        $response = $WPAC_Products->WPAC_MethodCreateProduct($decoded);
        if (isset($response[0]->data) && $response[0]->data != null) {
            // Instancia de Objeto WP_REST_Response
            $res = new WP_REST_Response($response[0]->data);
            $res->set_status(400);

            // Retorno status 200 y el JSON
            return [$res];
        }
        $discount_coupon = $WPAC_Products->WPAC_CreateCouponDiscount($response['product_id'], $decoded->price_smartfit);
        $response['coupon_id'] = $discount_coupon;
        // Instancia de Objeto WP_REST_Response
        $res = new WP_REST_Response($response);
        $res->set_status(200);

        // Retorno status 200 y el JSON
        return [$res];
    }

    /**
     * WPAC_MethodUpdateProduct Endpoint para Actualizr Producto
     * 
     * @access public
     * @return string
     */
    function WPAC_MethodUpdateProduct($request)
    {
        // Obtengo el Body del Request
        $data = $request->get_body();
        // Convierto el Body del Request en Array
        $decoded = json_decode($data);

        $WPAC_Products = new WPAC_Products();
        $response = $WPAC_Products->WPAC_MethodUpdateProduct($decoded);
        // if (isset($response[0]->data) && $response[0]->data != null) {
        //     // Instancia de Objeto WP_REST_Response
        //     $res = new WP_REST_Response($response[0]->data);
        //     $res->set_status(400);

        //     // Retorno status 200 y el JSON
        //     return [$res];
        // }

        // Instancia de Objeto WP_REST_Response
        $res = new WP_REST_Response($response);
        $res->set_status(200);

        // Retorno status 200 y el JSON
        return [$res];
    }

    /**
     * WPAC_MethodReadProduct Endpoint obtener informacion de un producto
     * 
     * @access public
     * @return string
     */
    function WPAC_MethodReadProduct($request)
    {
        $WPAC_Products = new WPAC_Products();
        return $WPAC_Products->WPAC_MethodReadProduct($request);
    }

    /**
     * WPAC_MethodCreateUser Endpoint para crear usuarios/clientes
     * 
     * @access public
     * @return string
     */
    function WPAC_MethodCreateUser($request)
    {
        // Obtengo el Body del Request
        $data = $request->get_body();
        // Convierto el Body del Request en Array
        $decoded = json_decode($data);

        $WPAC_Users = new WPAC_Users();

        $response = $WPAC_Users->WPAC_MethodCreateUser($decoded);
        if (isset($response[0]->data) && $response[0]->data != null) {
            // Instancia de Objeto WP_REST_Response
            $res = new WP_REST_Response($response[0]->data);
            $res->set_status(400);

            // Retorno status 200 y el JSON
            return [$res];
        }

        // Instancia de Objeto WP_REST_Response
        $res = new WP_REST_Response($response);
        $res->set_status(200);



        // Retorno status 200 y el JSON
        return [$res];
    }

    /**
     * WPAC_MethodReadUser Endpoint para obtener informacion usuarios/clientes
     * 
     * @access public
     * @return string
     */
    function WPAC_MethodReadUser($request)
    {
        // Obtengo el Body del Request
        $data = $request->get_body();
        // Convierto el Body del Request en Array
        $decoded = json_decode($data);

        $WPAC_Users = new WPAC_Users();

        $response = $WPAC_Users->WPAC_MethodReadUser($decoded);
        if (isset($response[0]->data) && $response[0]->data != null) {
            // Instancia de Objeto WP_REST_Response
            $res = new WP_REST_Response($response[0]->data);
            $res->set_status(400);

            // Retorno status 200 y el JSON
            return [$res];
        }

        // Instancia de Objeto WP_REST_Response
        $res = new WP_REST_Response($response);
        $res->set_status(200);



        // Retorno status 200 y el JSON
        return [$res];
    }

    /**
     * WPAC_MethodEditUser Endpoint para editar informacion usuarios/clientes
     * 
     * @access public
     * @return string
     */
    function WPAC_MethodEditUser($request)
    {
        // Obtengo el Body del Request
        $data = $request->get_body();
        // Convierto el Body del Request en Array
        $decoded = json_decode($data);

        $WPAC_Users = new WPAC_Users();

        $response = $WPAC_Users->WPAC_MethodEditUser($decoded);
        if (isset($response[0]->data) && $response[0]->data != null) {
            // Instancia de Objeto WP_REST_Response
            $res = new WP_REST_Response($response[0]->data);
            $res->set_status(400);

            // Retorno status 200 y el JSON
            return [$res];
        }

        // $response = $decoded;

        // Instancia de Objeto WP_REST_Response
        $res = new WP_REST_Response($response);
        $res->set_status(200);
        return [$res];
    }

    /**
     * WPAC_MethodReadOrdersUser Endpoint para obtener informacion de las ordenes
     * 
     * @access public
     * @return string
     */
    function WPAC_MethodReadOrdersUser($request)
    {
        // Obtengo el Body del Request
        $data = $request->get_body();
        // Convierto el Body del Request en Array
        $decoded = json_decode($data);

        $WPAC_Users = new WPAC_Users();

        $response = $WPAC_Users->WPAC_MethodReadOrdersUser($decoded);
        // if (isset($response[0]->data) && $response[0]->data != null) {
        //     // Instancia de Objeto WP_REST_Response
        //     $res = new WP_REST_Response($response[0]->data);
        //     $res->set_status(400);

        //     // Retorno status 200 y el JSON
        //     return [$res];
        // }

        // $response = $decoded;

        // Instancia de Objeto WP_REST_Response
        $res = new WP_REST_Response($response);
        $res->set_status(200);
        return [$res];
    }

    /**
     * WPAC_MethodCancelSuscriptionUser Endpoint para cancelar una suscripcion
     * 
     * @access public
     * @return string
     */
    function WPAC_MethodCancelSuscriptionUser($request)
    {
        // Obtengo el Body del Request
        $data = $request->get_body();
        // Convierto el Body del Request en Array
        $decoded = json_decode($data);

        $WPAC_Users = new WPAC_Users();
        $response = $WPAC_Users->WPAC_MethodCancelSuscriptionUser($decoded);

        // Instancia de Objeto WP_REST_Response
        $res = new WP_REST_Response($response);
        $res->set_status(200);
        return [$res];
    }


    /**
     * WPAC_MethodRecoverPassword Endpoint recuperar contrase;a de usuario
     * 
     * @access public
     * @return string
     */
    function WPAC_MethodRecoverPassword($request)
    {
        // Obtengo el Body del Request
        $data = $request->get_body();
        // Convierto el Body del Request en Array
        $decoded = json_decode($data);

        $WPAC_Users = new WPAC_Users();
        $response = $WPAC_Users->WPAC_MethodRecoverPassword($decoded);

        // Instancia de Objeto WP_REST_Response
        $res = new WP_REST_Response($response);
        $res->set_status(200);
        return [$res];
    }

    /**
     * WPAC_MethodRecoverPassword Endpoint para validar si usuario y contrase;a de cliente es correcta
     * 
     * @access public
     * @return string
     */
    function WPAC_SignUpUser($request)
    {
        // Obtengo el Body del Request
        $data = $request->get_body();
        // Convierto el Body del Request en Array
        $decoded = json_decode($data);

        $WPAC_Users = new WPAC_Users();
        $response = $WPAC_Users->WPAC_SignUpUser($decoded);

        // Instancia de Objeto WP_REST_Response
        $res = new WP_REST_Response($response);
        $res->set_status(200);
        return [$res];
    }
}

new WPAC_Custom_API;

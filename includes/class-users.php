<?php
/**
 * WPAC_Users Class para todos los metodos relacionados con Usuario
 * 
 * @access public
 */
class WPAC_Users
{
    function __construct()
    {
    }

    /**
    * WPAC_ValidateKeys Metodo para validar las Keys de seguridad
    * 
    * @access public
    */  
    public function WPAC_ValidateKeys($secret_key, $client_key)
    {
        $clientKey = get_option('WPAC_Client_Key');
        $secretKey = get_option('WPAC_Secret_Key');

        if ($clientKey == $client_key && $secretKey == $secret_key) {
            return true;
        }
        return false;
    }

    /**
    * WPAC_CreateUser Metodo para crear usuarios
    * 
    * @access public
    */  
    public function WPAC_CreateUser($email, $password, $document_number, $fullname, $document_type, $date_of_birth, $phone, $address, $departament, $province, $district, $sex)
    {
        $user_data = array(
            'user_pass'     => $password,
            'user_login'    => $document_number,
            'user_nicename' => $document_number,
            'first_name'    => $fullname,
            'user_email'    => $email,
            'display_name'  => $document_number,
            'role'          => 'customer',
        );
        $user_id = wp_insert_user($user_data);
        $user_data['user_id'] = $user_id;
        $body = '
        <!DOCTYPE html>
        <html lang="en">

        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Document</title>
        </head>

        <body style="margin: 0; padding: 0;">
            <div style="max-width: 900px; margin: 0 auto;">
                <header>
                <img style="width: 100%; height: auto;" src="https://i.imgur.com/5GZlvZ2.png" alt="welcome">
                </header>
                
                <p>Hola <strong>'. $fullname . '</strong></p>
                
                <p>Acabas de adquirir tu membresía para nuestra plataforma de coaching nutricional online. Podrás hacer tu
                evaluación, acceder a tus resultados y a todo nuestro contenido, recomendaciones y guías nutricionales para
                el
                logro de tus objetivos.</p>
                
                <p>Podrás acceder a la plataforma con los siguientes accesos.</p>
                
                <p style="margin-bottom: 4px;"><strong>Login: '.$email.' </strong></p>
                <p style="margin: 0;"><strong>Contraseña: '.$password.'</strong></p>
                <p>Podrás revisar el contrato adjunto a este correo.</p>
                
                <footer>
                <img style="width: 100%; height: auto;" src="https://i.imgur.com/tm2gDR9.png" alt="welcome">
                </footer>
            </div>

        </body>
        </html>
        ';
        $headers = array('Content-Type: text/html; charset=UTF-8');
        wp_mail($email, 'Bienvenido a Smartfit Nutri', $body, $headers);
        $WPBS_API_Smartfitgo = new WPBS_API_Smartfitgo();
        if ($WPBS_API_Smartfitgo->Is_User_Smartfitgo($document_number)) {
            update_user_meta($user_id, 'smartfit_client', 1);
            $user_data['smartfit_client'] = 'true';
        } else {
            update_user_meta($user_id, 'smartfit_client', 0);
            $user_data['smartfit_client'] = 'false';
        }
        $data = array(
            'billing_first_name'        => $fullname,
            'billing_email'             => $email,
            'billing_type_ci'           => $document_type,
            'billing_cedula'            => $document_number,
            'billing_fecha_nacimiento'  => $date_of_birth,
            'billing_phone'             => $phone,
            'billing_address_1'         => $address,
            'billing_address_2'         => $address,
            'billing_departamento'      => $departament,
            'billing_provincia'         => $province,
            'billing_distrito'          => $district,
            'billing_genero'            => $sex,

        );
        foreach ($data as $meta_key => $meta_value) {
            update_user_meta($user_id, $meta_key, $meta_value);
        }

        return $user_data;
    }

    /**
    * WPAC_EditUser Metodo para actualizar usuarios
    * 
    * @access public
    */  
    public function WPAC_EditUser($email = '', $fullname = '', $date_of_birth = '', $phone = '', $address = '', $departament = '', $province = '', $district = '', $sex = '')
    {
        $get_user_by_email = get_user_by('email', $email);

        if (!$get_user_by_email) {
            return 0;
        }

        if ($fullname != '') {
            update_user_meta($get_user_by_email->ID, 'billing_first_name', $fullname);
            update_user_meta($get_user_by_email->ID, 'first_name', $fullname);
        }
        if ($date_of_birth != '') {
            update_user_meta($get_user_by_email->ID, 'billing_fecha_nacimiento', $date_of_birth);
        }
        if ($phone != '') {
            update_user_meta($get_user_by_email->ID, 'billing_phone', $phone);
        }
        if ($address != '') {
            update_user_meta($get_user_by_email->ID, 'billing_address_1', $address);
            update_user_meta($get_user_by_email->ID, 'billing_address_2', $address);
        }
        if ($departament != '') {
            update_user_meta($get_user_by_email->ID, 'billing_departamento', $departament);
        }
        if ($province != '') {
            update_user_meta($get_user_by_email->ID, 'billing_provincia', $province);
        }
        if ($district != '') {
            update_user_meta($get_user_by_email->ID, 'billing_distrito', $district);
        }
        if ($sex != '') {
            update_user_meta($get_user_by_email->ID, 'billing_genero', $sex);
        }

        return $get_user_by_email->ID;
    }

    /**
    * WPAC_HandleErrorsCreateUser Metodo para manejo de errores al crear usuarios
    * 
    * @access public
    */  
    public function WPAC_HandleErrorsCreateUser($decoded)
    {

        if (!isset($decoded->email) || $decoded->email == '') {
            $res = new WP_REST_Response(['error' => 'email is invalid']);
            $res->set_status(400);

            // Retorno status 400
            return [$res];
        }

        $user = get_user_by('email', $decoded->email);
        if ($user != false) {
            $res = new WP_REST_Response(['error' => 'email already exists']);
            $res->set_status(400);

            // Retorno status 400
            return [$res];
        }
        if (!isset($decoded->document_number) || $decoded->document_number == '') {
            $res = new WP_REST_Response(['error' => 'document_number is invalid']);
            $res->set_status(400);

            // Retorno status 400
            return [$res];
        }
        $user_login = get_user_by('login', $decoded->document_number);
        if ($user_login != false) {
            $res = new WP_REST_Response(['error' => 'document_number already exists']);
            $res->set_status(400);

            // Retorno status 400
            return [$res];
        }
        if (!isset($decoded->document_type) || $decoded->document_type == '') {
            $res = new WP_REST_Response(['error' => 'document_type is invalid']);
            $res->set_status(400);

            // Retorno status 400
            return [$res];
        }

        if (!isset($decoded->fullname) || $decoded->fullname == '') {
            // Instancia de Objeto WP_REST_Response
            $res = new WP_REST_Response(array('error' => 'fullname is invalid'));
            $res->set_status(500);

            // Retorno status 500
            return [$res];
        }

        if (!isset($decoded->date_of_birth) || $decoded->date_of_birth == '') {
            $res = new WP_REST_Response(['error' => 'date_of_birth is invalid']);
            $res->set_status(400);

            // Retorno status 400
            return [$res];
        }

        if (!isset($decoded->phone) || $decoded->phone == '') {
            $res = new WP_REST_Response(['error' => 'phone is invalid']);
            $res->set_status(400);

            // Retorno status 400
            return [$res];
        }

        if (!isset($decoded->address) || $decoded->address == '') {
            $res = new WP_REST_Response(['error' => 'address is invalid']);
            $res->set_status(400);

            // Retorno status 400
            return [$res];
        }

        if (!isset($decoded->departament) || $decoded->departament == '') {
            $res = new WP_REST_Response(['error' => 'departament is invalid']);
            $res->set_status(400);

            // Retorno status 400
            return [$res];
        }

        if (!isset($decoded->province) || $decoded->province == '') {
            $res = new WP_REST_Response(['error' => 'province is invalid']);
            $res->set_status(400);

            // Retorno status 400
            return [$res];
        }

        if (!isset($decoded->district) || $decoded->district == '') {
            $res = new WP_REST_Response(['error' => 'district is invalid']);
            $res->set_status(400);

            // Retorno status 400
            return [$res];
        }

        if (!isset($decoded->sex) || $decoded->sex == '') {
            $res = new WP_REST_Response(['error' => 'sex is invalid']);
            $res->set_status(400);

            // Retorno status 400
            return [$res];
        }

        return false;
    }

    /**
    * WPAC_MethodCreateUser Metodo para validar keys y llamar metodo de crear usuarios
    * 
    * @access public
    */  
    public function WPAC_MethodCreateUser($decoded)
    {
        // Obtengo Keys de Seguridad
        $secret_key = $decoded->secret_key;
        $client_key = $decoded->client_key;
        // Valido Keys de Seguridad
        $KeysValidation = $this->WPAC_ValidateKeys($secret_key, $client_key);
        if (!$KeysValidation) {
            // Instancia de Objeto WP_REST_Response
            $res = new WP_REST_Response(array('error' => 'secret_key or client_key incorrect'));
            $res->set_status(500);

            // Retorno status 500
            return [$res];
        }

        // Valido Errores en el Request
        $HandleErrors = $this->WPAC_HandleErrorsCreateUser($decoded);
        if ($HandleErrors) {
            return $HandleErrors;
        }
        $user_data = $this->WPAC_CreateUser($decoded->email, $decoded->password, $decoded->document_number, $decoded->fullname, $decoded->document_type, $decoded->date_of_birth, $decoded->phone, $decoded->address, $decoded->departament, $decoded->province, $decoded->district, $decoded->sex);
        return $user_data;
    }

    /**
    * WPAC_MethodReadUser Metodo para obtener informacion del usuario
    * 
    * @access public
    */  
    public function WPAC_MethodReadUser($decoded)
    {
        // Obtengo Keys de Seguridad
        $secret_key = $decoded->secret_key;
        $client_key = $decoded->client_key;
        // Valido Keys de Seguridad
        $KeysValidation = $this->WPAC_ValidateKeys($secret_key, $client_key);
        if (!$KeysValidation) {
            // Instancia de Objeto WP_REST_Response
            $res = new WP_REST_Response(array('error' => 'secret_key or client_key incorrect'));
            $res->set_status(500);

            // Retorno status 500
            return [$res];
        }
        if (isset($decoded->document_number) && $decoded->document_number != '') {
            $user = get_user_by('billing_cedula', $decoded->document_number);
            if ($user != false) {
                $data = array(
                    'document_number'   =>  get_user_meta($user->ID, 'billing_cedula', true),
                    'document_type'     =>  get_user_meta($user->ID, 'billing_type_ci', true),
                    'fullname'          =>  get_user_meta($user->ID, 'billing_first_name', true),
                    'date_of_birth'     =>  get_user_meta($user->ID, 'billing_fecha_nacimiento', true),
                    'phone'             =>  get_user_meta($user->ID, 'billing_phone', true),
                    'address'           =>  get_user_meta($user->ID, 'billing_address_1', true) . get_user_meta($user->ID, 'billing_address_2', true),
                    'departament'       =>  get_user_meta($user->ID, 'billing_departamento', true),
                    'province'          =>  get_user_meta($user->ID, 'billing_provincia', true),
                    'district'          =>  get_user_meta($user->ID, 'billing_distrito', true),
                    'sex'               =>  get_user_meta($user->ID, 'billing_genero', true),
                    'email'             =>  $user->user_email,
                    'is_smartfit'       =>  get_user_meta($user->ID, 'smartfit_client', true) == 1 ? 'true' : 'false',
                );
                return $data;
            }
        }

        if (isset($decoded->email) && $decoded->email != '') {
            $user = get_user_by('email', $decoded->email);
            if ($user != false) {

                $data = array(
                    'document_number'   =>  get_user_meta($user->ID, 'billing_cedula', true),
                    'document_type'     =>  get_user_meta($user->ID, 'billing_type_ci', true),
                    'fullname'          =>  get_user_meta($user->ID, 'billing_first_name', true),
                    'date_of_birth'     =>  get_user_meta($user->ID, 'billing_fecha_nacimiento', true),
                    'phone'             =>  get_user_meta($user->ID, 'billing_phone', true),
                    'address'           =>  get_user_meta($user->ID, 'billing_address_1', true) . get_user_meta($user->ID, 'billing_address_2', true),
                    'departament'       =>  get_user_meta($user->ID, 'billing_departamento', true),
                    'province'          =>  get_user_meta($user->ID, 'billing_provincia', true),
                    'district'          =>  get_user_meta($user->ID, 'billing_distrito', true),
                    'sex'               =>  get_user_meta($user->ID, 'billing_genero', true),
                    'email'             =>  $user->user_email,
                    'is_smartfit'       =>  get_user_meta($user->ID, 'smartfit_client', true) == 1 ? 'true' : 'false',
                );
                return $data;
            }
        }

        // Instancia de Objeto WP_REST_Response
        $res = new WP_REST_Response(array('error' => 'User dont exists'));
        $res->set_status(500);

        // Retorno status 500
        return [$res];
    }

    /**
    * WPAC_MethodReadUser Metodo para validar keys y llamar metodo de actualizar usuarios
    * 
    * @access public
    */  
    public function WPAC_MethodEditUser($decoded)
    {
        // Obtengo Keys de Seguridad
        $secret_key = $decoded->secret_key;
        $client_key = $decoded->client_key;

        // Valido que las Keys sean Correctas
        $KeysValidation = $this->WPAC_ValidateKeys($secret_key, $client_key);
        if (!$KeysValidation) {
            // Instancia de Objeto WP_REST_Response
            $res = new WP_REST_Response();
            $res->set_status(500);

            // Retorno status 500
            return [$res];
        }

        $get_edit_user = $this->WPAC_EditUser($decoded->email, $decoded->fullname, $decoded->date_of_birth, $decoded->phone, $decoded->address, $decoded->departament, $decoded->province, $decoded->district, $decoded->sex);

        if ($get_edit_user == 0) {
            // Instancia de Objeto WP_REST_Response
            $res = new WP_REST_Response(array('error' => 'client dont exists'));
            $res->set_status(500);

            // Retorno status 500
            return [$res];
        }

        return array(
            "email"     =>  $decoded->email,
            "fullname"  =>  get_user_meta($get_edit_user, 'billing_first_name', true),
            "date_of_birth"  =>  get_user_meta($get_edit_user, 'billing_fecha_nacimiento', true),
            "phone"  =>  get_user_meta($get_edit_user, 'billing_phone', true),
            "address"  =>  get_user_meta($get_edit_user, 'billing_address_1', true),
            "departament"  =>  get_user_meta($get_edit_user, 'billing_departamento', true),
            "province"  =>  get_user_meta($get_edit_user, 'billing_provincia', true),
            "district"  =>  get_user_meta($get_edit_user, 'billing_distrito', true),
            "sex"  =>  get_user_meta($get_edit_user, 'billing_genero', true),
        );
    }
    /**
    * WPAC_MethodReadOrdersUser Metodo para validar keys y llamar metodo para obtener ordenes procesadas del usuario
    * 
    * @access public
    */  
    public function WPAC_MethodReadOrdersUser($decoded)
    {
        // Obtengo Keys de Seguridad
        $secret_key = $decoded->secret_key;
        $client_key = $decoded->client_key;

        // Valido que las Keys sean Correctas
        $KeysValidation = $this->WPAC_ValidateKeys($secret_key, $client_key);
        if (!$KeysValidation) {
            // Instancia de Objeto WP_REST_Response
            $res = new WP_REST_Response();
            $res->set_status(500);

            // Retorno status 500
            return [$res];
        }

        $email = $decoded->email;
        $user = get_user_by('email', $email);

        if (!$user) {
            // Instancia de Objeto WP_REST_Response
            $res = new WP_REST_Response(array('error' => 'User dont exists'));
            $res->set_status(500);

            // Retorno status 500
            return [$res];
        }
        $order_ids = wc_get_orders(array(
            // 'customer_id' => $user->ID,
            'customer' => $email,
            'status'      => ['wc-processing','wc-completed'],
            'return' => 'ids',
        ));
        $order_Array = []; 
        $countRecurrent =0;
        for ($i=0; $i < count($order_ids); $i++) { 
            $swAdd = true;
            $order_id = $order_ids[$i];
            $order = wc_get_order($order_id);
            $product_name = '';
            $product_sku = '';
            foreach ($order->get_items() as $item_id => $item) {
                $product_name = $item->get_name();
                $product_id = $item->get_product_id();
                $product = wc_get_product( $product_id );
                $product_sku = $product->get_sku();
            }
            $typeProductNiubiz = get_post_meta($product_id,"typeProductNiubiz",true);
            if($typeProductNiubiz == "recurrent"){
                $countRecurrent++;
                $swAdd = false;
            }
            if($countRecurrent <=1 || $swAdd){
                $order_Array[] = [
                    "Order_ID" => $order->get_id(),
                    "Price" => $order->get_total(),
                    "Date_Purchase" => $order->get_date_created()->date_i18n('Y-m-d'),
                    "Due_Date"  =>  get_post_meta($order->get_id(), '_expiration_date', true),
                    "Order_Status"  => $order->get_status(),
                    "Product"   => $product_name,
                    "Product_SKU"   => $product_sku,
                ];
            }
        }
        return $order_Array;
        // return $Order_Array;
    }

    /**
    * WPAC_MethodCancelSuscriptionUser Metodo para validar keys y llamar metodo de cancelar suscripcion
    * 
    * @access public
    */ 
    public function WPAC_MethodCancelSuscriptionUser($decoded)
    {
        // Obtengo Keys de Seguridad
        $secret_key = $decoded->secret_key;
        $client_key = $decoded->client_key;

        // Valido que las Keys sean Correctas
        $KeysValidation = $this->WPAC_ValidateKeys($secret_key, $client_key);
        if (!$KeysValidation) {
            // Instancia de Objeto WP_REST_Response
            $res = new WP_REST_Response();
            $res->set_status(500);

            // Retorno status 500
            return [$res];
        }

        $email = $decoded->email;
        $order_id = $decoded->order;
        $user = get_user_by('email', $email);

        if (!$user) {
            // Instancia de Objeto WP_REST_Response
            $res = new WP_REST_Response(array('error' => 'User dont exists'));
            $res->set_status(500);

            // Retorno status 500
            return [$res];
        }

        $order = new WC_Order($order_id);
        $order->update_status('cancelled', 'Se cancelo a traves de la API');

        $informationCancelled = [];
        $informationCancelled[] = [
            "Order_ID" => $order_id,
            'Order_Status' => 'Cancelled',
            'User' => $email
        ];





        $body = '
        <!DOCTYPE html>
        <html lang="en">

        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Document</title>
        </head>

        <body style="margin: 0; padding: 0;">
            <div style="max-width: 900px; margin: 0 auto;">
                <header>
                <img style="width: 100%; height: auto;" src="https://i.imgur.com/4qWAvBr.png" alt="welcome">
                </header>
                
                <p>Hola <strong>'. $user->first_name . '</strong></p>
                <p>Lamentamos que no puedas continuar con nosotros. De acuerdo a tu solicitud, tu suscripción queda cancelada.</p>
                <p>Estaríamos encantados de que volvieras con nosotros. Si cambias de opinión, solo tendrás que reactivar tu suscripción para disfrutar del mejor contenido sobre nutrición inteligente.</p>
                <p>Podrás revisar el documento de cancelación de plan adjunto a este correo.</p>
                
                <footer>
                <img style="width: 100%; height: auto;" src="https://i.imgur.com/tm2gDR9.png" alt="welcome">
                </footer>
            </div>

        </body>
        </html>
        ';
        $headers = array('Content-Type: text/html; charset=UTF-8');
        wp_mail($email, 'Bienvenido a Smartfit Nutri', $body, $headers);






        return $informationCancelled;
    }

    /**
    * WPAC_MethodRecoverPassword Metodo para validar keys y llamar metodo de recuperar contrase;a
    * 
    * @access public
    */ 
    public function WPAC_MethodRecoverPassword($decoded)
    {
        // Obtengo Keys de Seguridad
        $secret_key = $decoded->secret_key;
        $client_key = $decoded->client_key;

        // Valido que las Keys sean Correctas
        $KeysValidation = $this->WPAC_ValidateKeys($secret_key, $client_key);
        if (!$KeysValidation) {
            // Instancia de Objeto WP_REST_Response
            $res = new WP_REST_Response();
            $res->set_status(500);

            // Retorno status 500
            return [$res];
        }

        $email = $decoded->email;
        $new_password = $decoded->new_password;
        $user = get_user_by('email', $email);

        if (!$user) {
            // Instancia de Objeto WP_REST_Response
            $res = new WP_REST_Response(array('error' => 'User dont exists'));
            $res->set_status(500);

            // Retorno status 500
            return [$res];
        }
        wp_set_password($new_password, $user->ID);
        $informationNewPassword = [];
        $informationNewPassword[] = [
            'User' => $email,
            "change_password" => 'completed',
        ];

        return $informationNewPassword;
    }

    /**
    * WPAC_SignUpUser Metodo para validar keys y llamar metodo de recuperar inicio de sesion
    * 
    * @access public
    */ 
    public function WPAC_SignUpUser($decoded)
    {
        // Obtengo Keys de Seguridad
        $secret_key = $decoded->secret_key;
        $client_key = $decoded->client_key;

        // Valido que las Keys sean Correctas
        $KeysValidation = $this->WPAC_ValidateKeys($secret_key, $client_key);
        if (!$KeysValidation) {
            // Instancia de Objeto WP_REST_Response
            $res = new WP_REST_Response();
            $res->set_status(500);

            // Retorno status 500
            return [$res];
        }

        $email = $decoded->email;
        $password = $decoded->password;
        $user = get_user_by('email', $email);

        if (!$user) {
            // Instancia de Objeto WP_REST_Response
            $res = new WP_REST_Response(array('error' => 'User dont exists'));
            $res->set_status(500);

            // Retorno status 500
            return [$res];
        }

        $informationSignUp = [];
        if (wp_check_password($password, $user->data->user_pass, $user->ID)) {
            $informationSignUp[] = [
                'User' => $email,
                "signup" => 'valid',
            ];
        } else {
            $informationSignUp[] = [
                'User' => $email,
                "signup" => 'unauthorized',
            ];
        }

        return $informationSignUp;
    }
}

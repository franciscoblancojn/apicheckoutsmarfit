<?php

/** CLASE VALIDAR LOS CLIENT KEY Y SECRET KEY */
class WPAC_Activator
{
    public static function WPAC_Activate()
    {

        $WPAC_Client_Key = get_option('WPAC_Client_Key', false);
        $WPAC_Secret_Key = get_option('WPAC_Secret_Key', false);
        $WPAC_URL_Checkout = get_option('WPAC_URL_Checkout', false);
        $value = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";

        if (!$WPAC_Client_Key) {
            $value = md5(rand());
            add_option('WPAC_Client_Key', $value);
        }

        if (!$WPAC_Secret_Key) {
            $value = md5(rand());
            add_option('WPAC_Secret_Key', $value);
        }

        if (!$WPAC_URL_Checkout) {
            add_option('WPAC_URL_Checkout', $value);
        }

        if ($WPAC_URL_Checkout != false && $WPAC_URL_Checkout != $value) {
            update_option('WPAC_URL_Checkout', $value);
        }
    }
}

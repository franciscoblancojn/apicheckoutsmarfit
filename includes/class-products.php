<?php
/**
 * WPAC_Products Class para todos los metodos relacionados con Productos
 * 
 * @access public
 */
class WPAC_Products
{
    function __construct()
    {
    }

    /**
    * WPAC_ValidateKeys Metodo para validar las Keys de seguridad
    * 
    * @access public
    */  
    public function WPAC_ValidateKeys($secret_key, $client_key)
    {
        $clientKey = get_option('WPAC_Client_Key');
        $secretKey = get_option('WPAC_Secret_Key');

        if ($clientKey == $client_key && $secretKey == $secret_key) {
            return true;
        }
        return false;
    }

    /**
    * WPAC_createProduct Metodo para Crear producto
    * 
    * @access public
    */  
    public function WPAC_createProduct($name, $price, $sku, $category, $stock, $product_type, $time_available = '', $time_parameter = '', $free_days = '',  $free_days_validate = false, $is_payment, $addon_code)
    {
        // Creo el Post type Product
        $args = array(
            'post_title'    =>  $name,
            'post_type'     =>  'product',
            'post_status'   =>  'publish'
        );
        $post_id = wp_insert_post($args); // First we create the product post so we can grab it's ID
        wp_set_object_terms($post_id, 'simple', 'product_type'); // set product is simple/variable/grouped

        update_post_meta($post_id, '_regular_price', '');   // Price of Data
        update_post_meta($post_id, '_sku', $sku);    // SKU of Data
        update_post_meta($post_id, '_price', $price); // Price Product

        // Valido y defino 'category' como Categoría
        if ($category != '' && $category != null) {
            $term = term_exists($category);
            if ($term != null && $term != 0) {
                // echo __($category);
                wp_set_object_terms($post_id, intval($term), 'product_cat'); // set category product
            } else {
                $new_term = wp_insert_term($category, 'product_cat');
                wp_set_object_terms($post_id, $new_term['term_id'], 'product_cat'); // set category product
            }
        }

        // Almaceno la información del Producto
        update_post_meta($post_id, '_visibility', 'visible');
        update_post_meta($post_id, '_stock_status', 'instock');
        update_post_meta($post_id, 'total_sales', '0');
        update_post_meta($post_id, '_downloadable', 'no');
        update_post_meta($post_id, '_virtual', 'no');
        update_post_meta($post_id, '_sale_price', '');
        update_post_meta($post_id, '_purchase_note', '');
        update_post_meta($post_id, '_featured', 'no');
        update_post_meta($post_id, '_weight', '');
        update_post_meta($post_id, '_length', '');
        update_post_meta($post_id, '_width', '');
        update_post_meta($post_id, '_height', '');
        update_post_meta($post_id, '_product_attributes', array());
        update_post_meta($post_id, '_sale_price_dates_from', '');
        update_post_meta($post_id, '_sale_price_dates_to', '');
        update_post_meta($post_id, '_sold_individually', '');
        update_post_meta($post_id, '_manage_stock', 'yes'); // activate stock management
        update_post_meta($post_id, '_backorders', 'no');

        // Metadatos Especiales para API
        update_post_meta($post_id, '_product_type', $product_type);
        update_post_meta($post_id, '_time_available', $time_available);
        update_post_meta($post_id, '_time_parameter', $time_parameter);
        update_post_meta($post_id, '_addon_code', $addon_code);

        $_free_days_validate = $free_days_validate == true ? 'true' : 'false';
        $_free_days = $free_days_validate == true ? $free_days : '0';
        $_is_payment = $is_payment == true ? 'true' : 'false';

        update_post_meta($post_id, '_free_days_validate', $_free_days_validate);
        update_post_meta($post_id, '_free_days', $_free_days);
        update_post_meta($post_id, '_is_payment', $_is_payment);

        // Defino el Stock
        wc_update_product_stock($post_id, $stock, 'set'); // set 1000 in stock
        return $post_id;
    }

    /**
    * WPAC_updateProduct Metodo para actualizar producto
    * 
    * @access public
    */  
    public function WPAC_updateProduct($sku, $price = '', $name = '', $price_discount = '', $stock = '', $free_days_validate = false, $free_days = '')
    {
        $ID_Product = wc_get_product_id_by_sku($sku);
        if ($ID_Product == 0) {
            return false;
        }
        $product = wc_get_product($ID_Product);

        if ($name != '') {
            $post = get_post($ID_Product);
            $new_title = mb_convert_case($name, MB_CASE_TITLE, "UTF-8");
            // place the current post and $new_title into array
            $post_update = array(
                'ID'         => $ID_Product,
                'post_title' => $new_title
            );

            wp_update_post($post_update);
        }
        if ($price != '') {
            update_post_meta($ID_Product, '_price', $price); // Price Product
        }
        if ($price_discount != '') {
            $id_coupon = get_post_meta($ID_Product, '_discount_code_id', true);
            $new_amount = intval($price) - intval($price_discount);
            update_post_meta($id_coupon, 'coupon_amount', $new_amount);
        }
        if ($stock != '') {
            wc_update_product_stock($ID_Product, $stock, 'set'); // set 1000 in stock
        }
        $_free_days_validate = $free_days_validate == true ? 'true' : 'false'; // Price Product
        update_post_meta($ID_Product, '_free_days_validate', $_free_days_validate); // Price Product
        if ($free_days != '') {
            $_free_days = $free_days_validate == true ? $free_days : $free_days;
            update_post_meta($ID_Product, '_free_days', $_free_days); // Price Product
        }

        return $ID_Product;
    }

    /**
    * WPAC_getProductBySku Metodo para obtener informacion de producto segun su sku
    * 
    * @access public
    */  
    public function WPAC_getProductBySku($sku)
    {
        $ID_Product = wc_get_product_id_by_sku($sku);
        if ($ID_Product == 0) {
            return false;
        }

        $product = wc_get_product($ID_Product);
        $name = $product->get_name();
        $price = $product->get_price();
        $date_created = $product->get_date_created();
        $checkout_url = get_option('WPAC_URL_Checkout');
        $product_type = get_post_meta($ID_Product, '_product_type', true);
        $time_available = get_post_meta($ID_Product, '_time_available', true);
        $time_parameter = get_post_meta($ID_Product, '_time_parameter', true);
        $is_payment = get_post_meta($ID_Product, '_is_payment', true);
        $addon_code = get_post_meta($ID_Product, '_addon_code', true);

        $id_coupon = get_post_meta($ID_Product, '_discount_code_id', true);
        $amount_discount = get_post_meta($id_coupon, 'coupon_amount', true);
        $price_discount = $price - intval($amount_discount);

        $free_days_validate = get_post_meta($ID_Product, '_free_days_validate', true);
        $free_days = get_post_meta($ID_Product, '_free_days', true);

        $response = array(
            'checkout_url'           =>  $checkout_url . '/?add-to-cart=' . $ID_Product,
            'product_name'           =>  $name,
            'product_price'          =>  $price,
            'product_price_smartfit' =>  $price_discount,
            'product_id'             =>  $ID_Product,
            'product_sku'            =>  $sku,
            'free_days_validate'     =>  $free_days_validate,
            'free_days'              =>  $free_days,
            'product_type'           =>  $product_type,
            'is_payment'             =>  $is_payment,
            'addon_code'             =>  $addon_code
        );

        if ($product_type == 'recurrent' || $product_type == 'suscription') {
            $response['time_available'] = $time_available;
            $response['time_parameter'] = $time_parameter;
        }
        $response['date_created'] = $date_created;

        return $response;
    }

    /**
    * WPAC_HandleErrorsCreateProduct Metodo para manejar errores en el json de entrada al crear productos
    * 
    * @access public
    */  
    public function WPAC_HandleErrorsCreateProduct($decoded)
    {

        if (!isset($decoded->name) || $decoded->name == '') {
            $res = new WP_REST_Response(['error' => 'name is invalid']);
            $res->set_status(400);

            // Retorno status 400
            return [$res];
        }

        if (!isset($decoded->price) || $decoded->price == '') {
            $res = new WP_REST_Response(['error' => 'price is invalid']);
            $res->set_status(400);

            // Retorno status 400
            return [$res];
        }

        if (!isset($decoded->price_smartfit) || $decoded->price_smartfit == '') {
            // Instancia de Objeto WP_REST_Response
            $res = new WP_REST_Response(array('error' => 'price_smartfit is invalid'));
            $res->set_status(500);

            // Retorno status 500
            return [$res];
        }

        if (intval($decoded->price_smartfit) > intval($decoded->price)) {
            // Instancia de Objeto WP_REST_Response
            $res = new WP_REST_Response(array('error' => 'price_smartfit is greater than price'));
            $res->set_status(500);

            // Retorno status 500
            return [$res];
        }

        if (!isset($decoded->sku) || $decoded->sku == '') {
            $res = new WP_REST_Response(['error' => 'sku is invalid']);
            $res->set_status(400);

            // Retorno status 400
            return [$res];
        }

        if (wc_get_product_id_by_sku($decoded->sku) != 0) {
            $res = new WP_REST_Response(['error' => 'sku already exists']);
            $res->set_status(400);

            // Retorno status 400
            return [$res];
        }

        if (!isset($decoded->category) || $decoded->category == '') {
            $res = new WP_REST_Response(['error' => 'category is invalid']);
            $res->set_status(400);

            // Retorno status 400
            return [$res];
        }

        if (!isset($decoded->stock) || $decoded->stock == '') {
            $res = new WP_REST_Response(['error' => 'stock is invalid']);
            $res->set_status(400);

            // Retorno status 400
            return [$res];
        }

        if (!isset($decoded->free_days) || $decoded->free_days == '') {
            $res = new WP_REST_Response(['error' => 'free_days is invalid']);
            $res->set_status(400);

            // Retorno status 400
            return [$res];
        }

        if (!isset($decoded->addon_code) || $decoded->addon_code == '') {
            $res = new WP_REST_Response(['error' => 'addon_code is invalid']);
            $res->set_status(400);

            // Retorno status 400
            return [$res];
        }

        if (!isset($decoded->product_type) || $decoded->product_type == '') {
            $res = new WP_REST_Response(['error' => 'product_type is invalid']);
            $res->set_status(400);

            // Retorno status 400
            return [$res];
        }

        if ($decoded->product_type != 'simple' && $decoded->product_type != 'free_trial' && $decoded->product_type != 'variable' && $decoded->product_type != 'suscription' && $decoded->product_type != 'recurrent') {
            $res = new WP_REST_Response(['error' => 'product_type is invalid']);
            $res->set_status(400);

            // Retorno status 400
            return [$res];
        }

        if ($decoded->product_type == 'suscription' || $decoded->product_type == 'recurrent') {
            if (!isset($decoded->time_available) || $decoded->time_available == '') {
                $res = new WP_REST_Response(['error' => 'time_available is invalid']);
                $res->set_status(400);
                return [$res];
            }
            if (!isset($decoded->time_parameter) || $decoded->time_parameter == '') {
                $res = new WP_REST_Response(['error' => 'time_parameter is invalid']);
                $res->set_status(400);
                return [$res];
            }
        }
        return false;
    }

    /**
    * WPAC_HandleErrorsReadProduct Metodo para manejar errores en el json de entrada al consultar productos
    * 
    * @access public
    */ 
    public function WPAC_HandleErrorsReadProduct($decoded)
    {
        if (!isset($decoded->sku) || $decoded->sku == '') {
            $res = new WP_REST_Response(['error' => 'sku is invalid']);
            $res->set_status(400);

            // Retorno status 400
            return [$res];
        }

        if (wc_get_product_id_by_sku($decoded->sku) == 0) {
            $res = new WP_REST_Response(['error' => 'sku dont exists']);
            $res->set_status(400);

            // Retorno status 400
            return [$res];
        }

        return false;
    }

    /**
    * WPAC_MethodCreateProduct Metodo para validar keys y llamar metodo de crear producto
    * 
    * @access public
    */ 
    public function WPAC_MethodCreateProduct($decoded)
    {
        // Obtengo Keys de Seguridad
        $secret_key = $decoded->secret_key;
        $client_key = $decoded->client_key;
        // Valido Keys de Seguridad
        $KeysValidation = $this->WPAC_ValidateKeys($secret_key, $client_key);
        if (!$KeysValidation) {
            // Instancia de Objeto WP_REST_Response
            $res = new WP_REST_Response(array('error' => 'secret_key or client_key incorrect'));
            $res->set_status(500);

            // Retorno status 500
            return [$res];
        }

        // Valido Errores en el Request
        $HandleErrors = $this->WPAC_HandleErrorsCreateProduct($decoded);
        if ($HandleErrors) {
            return $HandleErrors;
        }

        // Defino las variables necesarias para crear producto
        $name = $decoded->name;
        $price = $decoded->price;
        $sku = $decoded->sku;
        $category = $decoded->category;
        $stock = $decoded->stock;
        $product_type = $decoded->product_type;
        $time_available = !isset($decoded->time_available) ? '' : $decoded->time_available;
        $time_parameter = !isset($decoded->time_parameter) ? '' : $decoded->time_parameter;
        $checkout_url = get_option('WPAC_URL_Checkout');
        $free_days_validate = $decoded->free_days_validate == false ? false : true;
        $free_days = $decoded->free_days;
        $is_payment = $decoded->is_payment;
        $addon_code = $decoded->addon_code;

        // Creo el Producto Simple
        $productID = $this->WPAC_createProduct($name, $price, $sku, $category, $stock, $product_type, $time_available, $time_parameter, $free_days, $free_days_validate, $is_payment, $addon_code);

        // Organizo Array de la Respuesta
        $response = array(
            'checkout_url'         =>  $checkout_url . '/?add-to-cart=' . $productID,
            'product_name'         =>  $name,
            'product_price'        =>  $price,
            'product_id'           =>  $productID,
            'free_days_validate'   =>  $free_days_validate,
            'free_days'            =>  $free_days,
            'product_type'         =>  $product_type,
            'is_payment'           =>  $is_payment,
            'addon_code'           =>  $addon_code
        );

        if ($product_type == 'recurrent' || $product_type == 'suscription') {
            $response['time_available'] = $time_available;
            $response['time_parameter'] = $time_parameter;
        }

        return $response;
    }

    /**
    * WPAC_MethodUpdateProduct Metodo para validar keys y llamar metodo de actualizar producto
    * 
    * @access public
    */ 
    public function WPAC_MethodUpdateProduct($decoded)
    {
        // Obtengo Keys de Seguridad
        $secret_key = $decoded->secret_key;
        $client_key = $decoded->client_key;

        // Valido que las Keys sean Correctas
        $KeysValidation = $this->WPAC_ValidateKeys($secret_key, $client_key);
        if (!$KeysValidation) {
            // Instancia de Objeto WP_REST_Response
            $res = new WP_REST_Response();
            $res->set_status(500);

            // Retorno status 500
            return [$res];
        }
        $product_id = $this->WPAC_updateProduct($decoded->sku, $decoded->price, $decoded->name, $decoded->price_smartfit, $decoded->stock, $decoded->free_days_validate, $decoded->free_days);
        $decoded->product_id = $product_id;

        return $decoded;

        // // Organizo Array de la Respuesta
        // $response = array(
        //     'decoded'  =>  $decoded
        // );

        // // Instancia de Objeto WP_REST_Response
        // $res = new WP_REST_Response($response);
        // $res->set_status(200);

        // // Retorno status 200 y el JSON
        // return [$res];
    }

    /**
    * WPAC_MethodReadProduct Metodo para validar keys y llamar metodo de consultar producto
    * 
    * @access public
    */ 
    public function WPAC_MethodReadProduct($request)
    {
        // Obtengo el Body del Request
        $data = $request->get_body();
        // Convierto el Body del Request en Array
        $decoded = json_decode($data);

        // Obtengo Keys de Seguridad
        $secret_key = $decoded->secret_key;
        $client_key = $decoded->client_key;

        $KeysValidation = $this->WPAC_ValidateKeys($secret_key, $client_key);
        if (!$KeysValidation) {
            // Instancia de Objeto WP_REST_Response
            $res = new WP_REST_Response(['error' => 'secret_key or client_key invalid']);
            $res->set_status(401);

            // Retorno status 500
            return [$res];
        }

        // Valido Errores en el Request
        $HandleErrors = $this->WPAC_HandleErrorsReadProduct($decoded);
        if ($HandleErrors) {
            return $HandleErrors;
        }

        $response = $this->WPAC_getProductBySku($decoded->sku);
        // Instancia de Objeto WP_REST_Response
        $res = new WP_REST_Response($response);
        $res->set_status(200);

        // Retorno status 200 y el JSON
        return [$res];
    }

    /**
    * WPAC_CreateCouponDiscount Metodo para crear cupones de descuento
    * 
    * @access public
    */ 
    public function WPAC_CreateCouponDiscount($product_id, $amount_discount)
    {

        $product = wc_get_product($product_id);
        $discount = $product->get_price() - intval($amount_discount);

        /**
         * Create a coupon programatically
         */
        $coupon_code = 'SMARTFIT_' . $product_id; // Code
        $amount = $discount; // Amount
        $discount_type = 'fixed_product'; // Type: fixed_cart, percent, fixed_product, percent_product

        $coupon = array(
            'post_title' => $coupon_code,
            'post_content' => '',
            'post_status' => 'publish',
            'post_author' => 1,
            'post_type' => 'shop_coupon'
        );

        $new_coupon_id = wp_insert_post($coupon);
        if ($new_coupon_id == 0 || $new_coupon_id == null) {
            // Instancia de Objeto WP_REST_Response
            $res = new WP_REST_Response(array('error' => 'error when generating the discount coupon'));
            $res->set_status(500);

            // Retorno status 500
            return [$res];
        }

        // Add meta
        update_post_meta($new_coupon_id, 'discount_type', $discount_type);
        update_post_meta($new_coupon_id, 'coupon_amount', $amount);
        update_post_meta($new_coupon_id, 'individual_use', 'no');
        update_post_meta($new_coupon_id, 'product_ids', $product_id);
        update_post_meta($new_coupon_id, 'exclude_product_ids', '');
        update_post_meta($new_coupon_id, 'usage_limit', '');
        update_post_meta($new_coupon_id, 'expiry_date', '');
        update_post_meta($new_coupon_id, 'apply_before_tax', 'yes');
        update_post_meta($new_coupon_id, 'free_shipping', 'no');

        update_post_meta($product_id, '_discount_code', $coupon_code);
        update_post_meta($product_id, '_discount_code_id', $new_coupon_id);

        return $new_coupon_id;
    }
}
